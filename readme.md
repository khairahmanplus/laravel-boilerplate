# Laravel Boilerplate

## Features
* Laravel Breadcrumbs
* Laravel Debugbar (Only enabled other than production environment)
* Artisan View (Only enabled other than production environment)

## FAQ

1. Link not generate properly?

    You need to edit `APP_URL` variable on `.env` file because every url is generated based on `APP_URL` variable

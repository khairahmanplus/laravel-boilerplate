@extends('layouts.dashboard')

@section('page-title')
Home
@endsection

@section('breadcrumbs')
{{ Breadcrumbs::render('home') }}
@endsection

@section('content')
You are logged in!
@endsection

@extends('layouts.app')

@section('content')
<form class="form-horizontal form-material" id="loginform" action="{{ route('register') }}" method="POST">

    {{ csrf_field() }}

    <h3 class="box-title m-b-20">Register</h3>

    <div class="form-group">
        <div class="col-xs-12">
            <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" placeholder="Name" value="{{ old('name') }}" required autofocus>
            @if ($errors->has('name'))
                <span class="invalid-feedback">
                    {{ $errors->first('name') }}
                </span>
            @endif
        </div>
    </div>

    <div class="form-group ">
        <div class="col-xs-12">
            <input class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" type="text" name="email" placeholder="Email" value="{{ old('email') }}" required>
            @if ($errors->has('email'))
                <span class="invalid-feedback">
                    {{ $errors->first('email') }}
                </span>
            @endif
        </div>
    </div>

    <div class="form-group ">
        <div class="col-xs-12">
            <input class="form-control" type="password" name="password" placeholder="Password" required>
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-12">
            <input class="form-control" type="password" name="password_confirmation" placeholder="Confirm Password" required>
        </div>
    </div>

    <div class="form-group text-center p-b-20">
        <div class="col-xs-12">
            <button class="btn btn-info btn-block btn-rounded text-uppercase waves-effect waves-light" type="submit">Register</button>
        </div>
    </div>

    <div class="form-group m-b-0">
        <div class="col-sm-12 text-center">
            Already have an account? <a href="{{ route('login') }}" class="text-info m-l-5"><b>Login</b></a>
        </div>
    </div>

</form>
@endsection

@extends('layouts.app')

@section('content')

    <form class="form-horizontal form-material" id="loginform" action="{{ route('login') }}" method="POST">

        {{ csrf_field() }}

        <h3 class="box-title m-b-20">Login</h3>

        <div class="form-group">
            <div class="col-xs-12">
                <input class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" type="email" name="email" placeholder="Email" value="{{ old('email') }}" required autofocus>
                @if ($errors->has('email'))
                    <span class="invalid-feedback">
                        {{ $errors->first('email') }}
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-12">
                <input class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" type="password" name="password" placeholder="Password" required>
                @if ($errors->has('password'))
                    <span class="help-block">
                        {{ $errors->first('password') }}
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-12">
                <div class="checkbox checkbox-info pull-left p-t-0">
                    <input class="filled-in chk-col-light-blue" id="checkbox-signup" type="checkbox" name="remember"  {{ old('remember') ? 'checked' : '' }}>
                    <label for="checkbox-signup">Remember me</label>
                </div>
                <a href="{{ route('password.request') }}" class="text-dark pull-right">
                    <i class="fa fa-lock m-r-5"></i>Forgot password?
                </a>
            </div>
        </div>

        <div class="form-group text-center">
            <div class="col-xs-12 p-b-20">
                <button class="btn btn-block btn-info btn-rounded" type="submit">Log In</button>
            </div>
        </div>

        <div class="form-group m-b-0">
            <div class="col-sm-12 text-center">
                Don't have an account? <a href="{{ route('register') }}" class="text-info m-l-5"><b>Register</b></a>
            </div>
        </div>

    </form>
@endsection

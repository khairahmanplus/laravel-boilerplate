@extends('layouts.app')

@section('content')
<form class="form-horizontal form-material" action="{{ route('password.request') }}" method="POST">
    {{ csrf_field() }}

    <div class="form-group ">
        <div class="col-xs-12">
            <h3>Reset Password</h3>
            <p class="text-muted">Enter your Email and a new password! </p>
        </div>
    </div>

    <input type="hidden" name="token" value="{{ $token }}">

    <div class="form-group">
        <input id="email" type="email" class="form-control" name="email" placeholder="Email" value="{{ $email or old('email') }}" required autofocus>
    </div>

    <div class="form-group">
        <input id="password" type="password" class="form-control" placeholder="New Password" name="password" required>
    </div>

    <div class="form-group">
        <input id="password-confirm" type="password" class="form-control" placeholder="New Password Confirmation" name="password_confirmation" required>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary btn-block">
            Reset Password
        </button>
    </div>
</form>
@endsection

@extends('layouts.app')

@section('content')
@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif

<form class="form-horizontal form-material" action="{{ route('password.email') }}" method="POST">

    {{ csrf_field() }}

    <div class="form-group ">
        <div class="col-xs-12">
            <h3>Recover Password</h3>
            <p class="text-muted">Enter your Email and instructions will be sent to you! </p>
        </div>
    </div>

    <div class="form-group ">
        <div class="col-xs-12">
            <input class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" type="email" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
            @if ($errors->has('email'))
                <span class="invalid-feedback">
                    {{ $errors->first('email') }}
                </span>
            @endif
        </div>
    </div>

    <div class="form-group text-center m-t-20">
        <div class="col-xs-12">
            <button class="btn btn-primary btn-block text-uppercase waves-effect waves-light" type="submit">Send Password Reset Link</button>
        </div>
    </div>

</form>
@endsection

<?php
// This is where you define any helpers function
// Please have a look first at Laravel's Helper Function Documentation
// https://laravel.com/docs/5.5/helpers
// to avoid any duplicated helpers function

if (! function_exists('set_active')) {
    /**
     * Set Active for Navigation Link
     * You can pass a single string to a route or multiple and wildcards
     * <a class="{{ set_active(['admin/institutes*','admin/courses*']) }}">Learning</a>
     *
     * @param string $path
     * @param string $active
     * @return string
     */
    function set_active($path, $active = 'active')
    {
        return call_user_func_array('Request::is', (array)$path) ? $active : '';
    }
}
